﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      do
      {
    	  // Initialisierung
    	  int millisekunde = 250;
          warte(millisekunde);
          //Ticketart	      
	      byte fahrkartenArt = fahrkartenbestellungErfassen(tastatur);
	      // Ticketanzahl und zuzahlender Betrag
	      double ticketpreis = ticketArt(tastatur, fahrkartenArt);
	      double zuZahlenderBetrag = zuzahlenderBetrag(tastatur, ticketpreis);
	      //Bezahlung
	      double rückgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
	      // Fahrscheinausgabe
	      fahrkartenAusgabe();
	      // Wartezeit
	      warte(millisekunde);
	      // Rückgeldausgabe
	      rueckgeldAusgeben(rückgabebetrag);
      }
      while(true);
       
    }

    	  //Ticketart
    	  
    public static byte fahrkartenbestellungErfassen(Scanner tastatur) {
    	byte fahrkartenArt = 0;
    	System.out.println("Guten Tag.\n");
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
    	System.out.println("	Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
    	System.out.println("	Tageskarte Regeltarif AB [8,60 EUR] (2)");
    	System.out.println("	Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	System.out.print("Ihre Wahl (1, 2 oder 3): ");
    	fahrkartenArt = tastatur.nextByte();
    	return fahrkartenArt;
    }
       
       //Art der Tickets und Ticketpreis
       public static double ticketArt(Scanner tastatur, byte fahrkartenArt) {
    	   double ticketpreis = 0.0;

	     //Ticketpreis
	   	if (fahrkartenArt == 1) {
	   		ticketpreis = 2.90;	
	   	}
	   	else if (fahrkartenArt == 2) {
	   		ticketpreis = 8.60;
	   	}
	   	else if (fahrkartenArt == 3) {
	   		ticketpreis = 23.50;
	   	}
	   	else if ((fahrkartenArt < 1) || (fahrkartenArt > 3)){
	   		System.out.println("	>>falsche Eingabe<<");

	   	}
	   		return ticketpreis;
	   		
	   	}
	  //Anzahl der Tickets und zuzahlenderBetrag
	   	public static double zuzahlenderBetrag(Scanner tastatur, double ticketpreis) {
	   	System.out.print("Anzahl der Tickets: ");
	   	byte ticketAnzahl = tastatur.nextByte();
	    double zuZahlenderBetrag = 0.0;
       
       if (ticketAnzahl <= 10) {
    	   zuZahlenderBetrag = ticketpreis*ticketAnzahl;// geht auch als "zuZahlenderBetrag *= anzahlTickets"
       }
       else if ((ticketAnzahl > 10) || (ticketAnzahl <= 0)){
    	   System.out.println("Ungültige Eingabe.");
    	   System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus: ");
    	   ticketAnzahl = tastatur.nextByte();
    	   zuZahlenderBetrag = ticketpreis * ticketAnzahl;// geht auch als "zuZahlenderBetrag *= anzahlTickets"
       }
       System.out.println("Zu zahlender Betrag: " + String.format("%1.2f Euro", zuZahlenderBetrag));
       return zuZahlenderBetrag;
    }       
   
       // Geldeinwurf - Fahrkarten bezahlen
       // -----------
    public static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
    	  double eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.println("Noch zu zahlen: " + String.format("%1.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag)));// auch möglich: System.out.printf("Noch zu zahlen: " + String.format("%1.2f Euro\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   double eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
           double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
           return rückgabebetrag;
           
       }

       // Fahrscheinausgabe
       // -----------------
       public static void fahrkartenAusgabe() {
       System.out.println("\nFahrschein wird ausgegeben");
       }
       
       // Wartezeit
       public static void warte(int millisekunde) {
    	   for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
       }
      
       // Rückgeldberechnung und -ausgabe
       // -------------------------------
       public static void rueckgeldAusgeben(double rückgabebetrag) {
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%1.2f", rückgabebetrag) + " EURO"); // auch möglich: System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
       }
       		while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  muenzeAusgeben(2, " EURO");
	          rückgabebetrag -= 2.0;
	        }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	   muenzeAusgeben(1, " EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	   muenzeAusgeben(50, " CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	   muenzeAusgeben(20, " CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	   muenzeAusgeben(10, " CENT");
	          rückgabebetrag -= 0.1;
           }
          while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  muenzeAusgeben(5, " CENT");
 	          rückgabebetrag -= 0.05;
           }
          System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
 	                "vor Fahrtantritt entwerten zu lassen!\n"+
 	                "Wir wünschen Ihnen eine gute Fahrt.\n");

       }
           
           public static void muenzeAusgeben(int betrag, String einheit) {
        	   System.out.println(betrag + einheit);
    	   
       }
	

       
       //5. Begründen Sie Ihre Entscheidung für die Wahl des Datentyps.
       //für den Ticketpreis habe ich double gewählt, da er als Gleitkommazahl ein- und ausgegeben werden soll. 
       //Für die Ticketzahl habe ich byte gewählt, da Ticketanzahlen i.d.R im niedrigen ein- oder zweistelligen Bereich bleiben.
       //6. Erläutern Sie detailliert, was bei der Berechnung des Ausdrucks anzahl * einzelpreis passiert.
       // Das Programm multipliziert die Eingabe zur Ticketanzahl mit der Eingabe des Einzelpreises und initialisiert die Variable "zuZahlenderBetrag" mit dem Ergebnis der Multiplikation.
}