import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString(myScanner);
		int anzahl = liesInt(myScanner);
		double preis = liesDouble(myScanner);
		double mwst = liesmwst(myScanner);

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis,mwst);

		// Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
	}
	public static String liesString(Scanner myScanner) {
		System.out.println("was m�chten Sie bestellen?");
		String artikel = myScanner.next();
		return artikel;
	}
	public static int liesInt(Scanner myScanner) {
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		return anzahl;
		}
	public static double liesDouble(Scanner myScanner) {
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	public static double liesmwst(Scanner myScanner) {
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
		return mwst;
}
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	
	}
}