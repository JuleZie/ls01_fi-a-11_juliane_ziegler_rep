import java.util.ArrayList;

public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktoren Raumschiff
	public Raumschiff() {
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	this.energieversorgungInProzent = energieversorgungInProzent;
	this.schildeInProzent = schildeInProzent;
	this.huelleInProzent = huelleInProzent;
	this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	this.androidenAnzahl = androidenAnzahl;
	this.schiffsname = schiffsname;
	}
	//Verwaltungsmethoden
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return this.schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	//Weitere Methoden
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);		
	}
	
	
}
