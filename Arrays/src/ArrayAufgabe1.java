
public class ArrayAufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges Array der L�nge 10 deklariert wird. 
		Anschlie�end wird das Array mittels Schleife mit den Zahlen von 0 bis 9 gef�llt. 
		Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer Schleife auf der Konsole aus.*/
		
		// Programm schreiben
		int [] zahlen = new int[10];//L�nge des Arrays in eckigen Klammern angeben
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print("Index " + i + ":" + zahlen[i] + ", ");
		}
		System.out.println("Array danach:");
		//Array mittels Schleife mit den Zahlen von 0 bis 9 f�llen
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i; //i wird in der Vorschleife erh�ht
			System.out.print("Index: " + i + ":" + zahlen[i] + ", ");
		}
		System.out.println("Array danach:");	
		//Array mittels Schleife mit den Zahlen von 1 bis 10 f�llen
			for (int i = 0; i < zahlen.length; i++) {
				zahlen[i] = i+1; //i wird in der Vorschleife erh�ht
				System.out.print("Index: " + i + ":" + zahlen[i] + ", ");
			}
		}
		
	}


