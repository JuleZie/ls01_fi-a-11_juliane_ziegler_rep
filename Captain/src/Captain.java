
public class Captain {

	private String surname;
	private int captainYears;
	private double gehalt;
	
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.
	private String name;
	public Captain() {
	}
	// TODO: 4. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen
	// initialisiert.
	public Captain(String surname, String name) {
		this.surname = surname;
		this.name = name;		
	}
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.
	public Captain(String surname, String name, int captainYears, double gehalt) {
		this.surname = surname;
		this.captainYears = captainYears;
		this.gehalt = gehalt;
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setGehalt(double gehalt) {
	// TODO: 1. Implementieren Sie die entsprechende set-Methode.
	// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		if(gehalt>0)
			this.gehalt = gehalt;

	}

	public double getGehalt() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		return this.gehalt;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;	
	}
	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears.
	// Ber�cksichtigen Sie, dass das Jahr nach Christus sein soll.
	public void setCaptainYears(int captainYears) {
		if(captainYears>0)
			this.captainYears = captainYears;
	}
	public int getCaptainYears() {
		return this.captainYears;
	}

	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String vollname() {
		String vollname = surname + " " + name;
		return vollname;
	}
	//TODO: 8. ToString-Methode implementieren
	public String toString() {
		return "Vorname: " + surname + " Name: " + name + " Kapit�nIn seit: " + captainYears + " Gehalt: " + gehalt;
	}

}