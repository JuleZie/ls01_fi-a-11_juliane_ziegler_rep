//import Captain;

public class TestCaptain {

	public static void main(String[] args) {
		//Erzeugen der Objekte
				Captain cap1 = new Captain ( "Jean Luc", "Picard", 2364, 4500.0);
				Captain cap2 = new Captain ( "Azetbur", "Gorkon", 2373, 0.0);
			    /* TODO: 9. Erzeugen Sie ein zusaetzliches Objekt cap3 und geben Sie es auch auf der Konsole aus, 
			     * die Attributwerte denken Sie sich aus.
			     */  
				Captain cap3 = new Captain ( "Jonathan", "Archer", 2150, 40000.0);
				System.out.println(cap3.toString());
			      
			    	      
			    /*TODO: 10. Erzeugen Sie zwei zusaetzliche Objekte cap4 und cap5
			     * mit dem Konstruktor, der den Namen und Vornamen initialisiert,
			     * die Attributwerte denken Sie sich aus.
			     */
			    Captain cap4 = new Captain ( "James Tiberius", "Kirk", 0, 0.0);
			    Captain cap5 = new Captain ( "Kathryn", "Janeway", 0, 0.0);
			    
			    //Setzen der Attribute
			    /* TODO: 11. Fuegen Sie cap4 und cap5 jeweils ein Gehalt hinzu, 
			     * die Attributwerte denken Sie sich aus.
			     * Geben Sie cap4 und cap5 auch auf dem Bildschirm aus.
			     */
			    cap4.setGehalt(40000.0);
			    cap5.setGehalt(60000.0);
			    			  	 
		        //Bildschirmausgabe
			    System.out.println("Name: " + cap1.getName());
			    System.out.println("Vorname: " + cap1.getSurname());
			    System.out.println("Kapit�n seit: " + cap1.getCaptainYears());
			    System.out.println("Gehalt: " + cap1.getGehalt() + " F�derationsdukaten");
			    System.out.println("Vollname: " + cap1.vollname());
			    System.out.println(cap1 + cap1.toString() + "\n"); //Die toString() Methode wird aufgerufen
			    
		        System.out.println("\nName: " + cap2.getName());
			    System.out.println("Vorname: " + cap2.getSurname());
			    System.out.println("Kapit�n seit: " + cap2.getCaptainYears());
			    System.out.println("Gehalt: " + cap2.getGehalt() + " Darsek");
			    System.out.println("Vollname: " + cap2.vollname());
			    System.out.println(cap2 + cap2.toString() + "\n"); //Die toString() Methode wird aufgerufen

			    //TODO: Ausgabe von cap4, erg�nzen Sie die fehlenden Methodenaufrufen
			    System.out.println("Name: " + cap4.getName());
			    System.out.println("Vorname: " + cap4.getSurname());
			    System.out.println("Kapit�n seit: " + cap4.getCaptainYears());
			    System.out.println("Gehalt: " +  cap4.getGehalt() + " F�derationsdukaten");
			    System.out.println("Vollname: " + cap4.vollname());
			    System.out.println(cap4 + cap4.toString() + "\n"); //Die toString() Methode wird aufgerufen
			    
			    //TODO: Ausgabe von cap5, erg�nzen Sie die fehlenden Methodenaufrufen
			    System.out.println("Name: " + cap5.getName());
			    System.out.println("Vorname: " + cap5.getSurname());
			    System.out.println("Kapit�n seit: " + cap5.getCaptainYears());
			    System.out.println("Gehalt: " +  cap5.getGehalt() + " F�derationsdukaten");
			    System.out.println("Vollname: " + cap5.vollname());
			    System.out.println(cap5 + cap5.toString() + "\n"); //Die toString() Methode wird aufgerufen
			    
			}

		}