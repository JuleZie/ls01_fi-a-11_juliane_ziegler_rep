
public class Konsolenausgabe1 {
	public static void main(String[] args) {
		// Konsolenausgabe �bung 1 Aufgabe 1

		System.out.println("Heute ist Freitag.");
		System.out.print("Morgen ist Samstag!");
		
		System.out.println("Sie sagte: \"Heute ist Freitag.\"");
		System.out.print("Morgen ist Samstag!\n");
		
		// "Heute ist Tag 25 des Monats." ausgeben
		int nummer = 25;
		String name = "Heute";
		System.out.println("" + name + " ist Tag " + nummer + " des Monats.");
		
		// Der Unterschied zwischen print() und println() ist, dass Letztere einen Zeilenvorschub durchf�hrt und der n�chste Text in der n�chsten Zeile ausgegeben wird.
		
		// Aufgabe 2

		String s = "*************";
		System.out.printf( "%9.1s\n", s );
		System.out.printf( "%10.3s\n", s );
		System.out.printf( "%11.5s\n", s );
		System.out.printf( "%12.7s\n", s );
		System.out.printf( "%13.9s\n", s );
		System.out.printf( "%14.11s\n", s );
		System.out.printf( "%15.13s\n", s );
		System.out.printf( "%14.11s\n", s );
		System.out.printf( "%13.9s\n", s );
		System.out.printf( "%12.7s\n", s );
		System.out.printf( "%10.3s\n", s );
		System.out.printf( "%10.3s\n", s );
		
		//Aufgabe 3
		//<terminated> Aufgabe3 [Java Application]
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf( "%.2f\n" , a);
		System.out.printf( "%.2f\n" , b);
		System.out.printf( "%.1f\n" , c);
		System.out.printf( "%.2f\n" , d);
		System.out.printf( "%.2f\n" , e);
		
		System.out.printf("%-5s = %-18s = %4d\n", "0!", "", 1);
		System.out.printf("%-5s = %-18s = %4d\n", "1!", "1", 1);
		System.out.printf("%-5s = %-18s = %4d\n", "2!", "1 * 2", 1 * 2);
		System.out.printf("%-5s = %-18s = %4d\n", "3!", "1 * 2 * 3", 1 * 2 * 3);
		System.out.printf("%-5s = %-18s = %4d\n", "4!", "1 * 2 * 3 * 4", 1 * 2 * 3 * 4);
		System.out.printf("%-5s = %-18s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5", 1 * 2 * 3 * 4 * 5);
}
}

