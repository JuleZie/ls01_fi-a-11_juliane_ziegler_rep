
public class WeltderZahlen {

	public static void main(String[] args) {
		 
	    /*  *********************************************************
	    
	         Zuerst werden die Variablen mit den Werten festgelegt!
	    
	    *********************************************************** */
	    // Im Internet gefunden ? ja
	    // Die Anzahl der Planeten in unserem Sonnesystem                    
	    byte anzahlPlaneten = 8;
	    
	    // Anzahl der Sterne in unserer Milchstra�e - 100-400 Milliarden
	    long anzahlSterne = 400000000000l;
	    // Wie viele Einwohner hat Berlin?
	    int bewohnerBerlin = 3664088;
	    
	    // Wie alt bist du? - 28 + 276 Tage. Wie viele Tage sind das?
	    short alterTage = 10496;
	    
	    // Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
	    int gewichtKilogramm = 190000;
	    
	    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
	    int flaecheGroessteLand = 17098242;
	    
	    // Wie gro� ist das kleinste Land der Erde?
	    
	    double flaecheKleinsteLand = 0.44;
	    
	    /*  *********************************************************
	    
	         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
	    
	    *********************************************************** */
	    
	    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
	    
	    System.out.println("Anzahl der Sterne: " + anzahlSterne);
	    System.out.println("Einwohner Berlin: " + bewohnerBerlin);
	    System.out.println("Mein Alter in Tagen: " + alterTage);
	    System.out.println("Das schwerste Tier der Welt wiegt: " + gewichtKilogramm + "Kg");
	    System.out.println("Das gr��te Land der Welt hat: " + flaecheGroessteLand + "Km2");
	    System.out.println("Das kleinste Land der Welt hat: " + flaecheKleinsteLand + "Km2");
	    
	    System.out.println(" *******  Ende des Programms  ******* ");
	   

	}

}
