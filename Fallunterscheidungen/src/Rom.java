import java.util.Scanner;
public class Rom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		char zahl;
		System.out.println("Dieses Programm gibt die Dezimalzahl f�r EIN r�misches Zahlreichen aus.");
		System.out.println("Bitte geben Sie ein r�misches Zahlzeichen ein: ");
		zahl = tastatur.next().charAt(0);
		
		if(zahl == 'I') {
			System.out.println("I = 1");
		}
		else if(zahl == 'V') {
			System.out.println("V = 5");
		}
		else if(zahl == 'X') {
			System.out.println("X = 10");
		}
		else if(zahl == 'L') {
			System.out.println("L = 50");
		}
		else if(zahl == 'C') {
			System.out.println("C = 100");
		}
		else if(zahl == 'D') {
			System.out.println("D = 500");
		}
		else if(zahl == 'M') {
			System.out.println("M = 1000");
		}
		else {
			System.out.println("F�r dieses Zeichen gibt es keinen r�mischen Zahlwert.");
		}

	}

}
