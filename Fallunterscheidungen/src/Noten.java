import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte note;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Diese Anwendung gibt die sprachliche Umschreibung f�r die Noten 1 -6 aus.");
		System.out.println("Bitte geben Sie eine Note (1 - 6) ein: ");
		note = tastatur.nextByte();
		
		if(note == 1) {
			System.out.println("1 = Sehr gut.");
		}
		
		if(note == 2) {
			System.out.println("2 = Gut.");
		}
		if(note == 3) {
			System.out.println("3 = Befriedigend.");
		}
		if(note == 4) {
			System.out.println("4 = Ausreichend.");
		}
		if(note == 5) {
			System.out.println("5 = Mangelhaft.");
		}
		if(note == 6) {
			System.out.println("6 = Ungen�gend");
		}
		else if (note > 6 || note < 1){
			System.out.println("Ung�ltige Eingabe.");
		}
	}

}
